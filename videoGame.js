//create a variable to store the data from other directory
const usersData = require('./users.cjs')

// create a variable to store the who plays the video-game by using filter method
const videoGamePlayers = Object.keys(usersData).filter((user)=>{
    return usersData[user].interests[0].includes("Video Games" || "Playing Video Game");
});
// it prints the output
console.log(videoGamePlayers);