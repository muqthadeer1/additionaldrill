// create a variable to access the imported data from other file
const usersData = require('./users.cjs')



// create a variable and find the user whose qualification is "Master" by using reduced method
const findWithMasterDegree = Object.keys(usersData).reduce((accumulator,currentValue)=>{
    if(usersData[currentValue].qualification === "Masters")
    {
        accumulator.push(currentValue);
    }
    return accumulator;
},[]);

//it prints output
console.log(findWithMasterDegree);

