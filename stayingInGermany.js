//create a variable to store the data from other directory
const usersData = require('./users.cjs')

// create a variable to find how many users are living in germany by filter method
const stayingInGermany = Object.keys(usersData).filter((user)=>{
     if(usersData[user].nationality==="Germany"){
        return user;
     }
});

// it prints the output
console.log(stayingInGermany);