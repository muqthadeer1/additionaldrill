//create a variable to store the data from other directory
const usersData = require("./users.cjs");


// Sort user names based on seniority and age
const sortedUsers = Object.keys(usersData).sort((a, b)=>{
    // create a object name seniorityOrder and give points accordingly as per seniority
  const seniorityOrder = {
    'Intern': 1,
    'Python': 2,
    'Senior': 3,
  };
  

//   Compare based on seniority first
  const seniorityA = seniorityOrder[usersData[a].desgination.split(' ')[0] || 0];
  const seniorityB = seniorityOrder[usersData[b].desgination.split(' ')[0] || 0];
  
  //if seniority is not Equal
  if (seniorityA !== seniorityB) { 
    return seniorityB - seniorityA;
  }

  // If seniority is the same, compare based on age
  else{
  const ageA = usersData[a].age || 0;
  const ageB = usersData[b].age || 0;
  return ageB - ageA;

  }
});

// it prints output
console.log(sortedUsers);
