//create a variable to store the data from other file
const usersData = require("./users.cjs");

// create  a variable to store the filtered data
const findGroup = Object.keys(usersData).reduce((acc, currentValue) => {
  // If user uses the programming language as "Golang"

  if (usersData[currentValue].desgination.includes("Golang")) {
    //if Golang is not present in Accumulator object then it insert Golang with Empty Array
    if (!acc.Golang) {
      acc.Golang = [];
    }
    //it pushes the Golang users in object(accumulator)
    acc.Golang.push(currentValue);
  }
  // If user uses the programming language as "Javascript"
  else if (usersData[currentValue].desgination.includes("Javascript")) {
    //if Javascript is not present in Accumulator object then it insert Javascript with Empty Array
    if (!acc.Javascript) {
      acc.Javascript = [];
    }
    //it pushes the Javascript users in object(accumulator)

    acc.Javascript.push(currentValue);
  }

  // If user uses the programming language as "Python"
  else {
    //if Python is not present in Accumulator object then it insert Python with Empty Array
    if (!acc.Python) {
      acc.Python = [];
    }
    //it pushes the python users in object(accumulator)
    acc.Python.push(currentValue);
  }
  //it return accumulator as Object
  return accumulator;
}, {});

//it prints output
console.log(findGroup);
